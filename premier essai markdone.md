# le titre de la premiere section

## le titre de la 2eme ss section
_italique_ *italique* 
__gras__ **gras**
échasse fixeé 'chasse fixe' `chasse fixe`
--barré-- ~~barrer~~

un hyperline [Élaboration et conversion de documents avec Markdown et Pandoc](https://enacit1.epfl.ch/markdown-pandoc/)

<!-- un commentaire -->
![une photo de chat](https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Collage_of_Six_Cats-02.jpg/290px-Collage_of_Six_Cats-02.jpg)

liste à puce
- +1er élément
- 2 eme element
- 3 eme element

numérotation
1. premier
2. deuxieme
3. ...

Je peux imbriquer
1. ff
    - a
    - b
2. ggg
3. on continu
    1. au début
    2. ensuite


on va s'arrêter là



# Nouvelle Section de Niveau 1

- liste de Voitures
    1. Porsche
    2. Ferrari
- Liste d'éditeurs
    1. Notepad++
    2. TextEdit

## Nouvelle Section de Niveau 2

Bonjour, c'est ma nouvelle Section de niveau 2,
qui permet de créer un heading de niveau 2 en HTML.


### C'est un paragraphe formé de plusieurs lignes 

<p> Ligne 1 du paragraphe <br> <hr>Ligne 2 du paragraphe <br> <hr> Ligne 3 du paragraphe
</p>
<br>

*Texte en Italique*  <br>

__texte en gras__ <br>

___texte en italique et gras___

<br>
[Lien vers Google](https://www.google.fr/)
<br>

![Image ](http://animtoit.fr/exemple-chien/)




    


